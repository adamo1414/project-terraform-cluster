
resource "azurerm_resource_group" "terraform1" {
  name                                = "${var.name_resource_groupe1}"
  location                            = "${var.location_group_resource1}"
}
#resouce group de la vm 5 au japon
resource "azurerm_resource_group" "terraform2" {
  name                                = "${var.name_resource_groupe2}"
  location                            = "${var.location_group_resource2}"
}

resource "azurerm_virtual_network" "terraform1" {
  name                                = "${var.name_virtual_network_terraform1}"
  address_space                       = ["10.0.0.0/16"]
  location                            = "${azurerm_resource_group.terraform1.location}"
  resource_group_name                 = "${azurerm_resource_group.terraform1.name}"
}

resource "azurerm_virtual_network" "terraform2" {
  name                                = "${var.name_virtual_network_terraform2}"
  address_space                       = ["10.1.0.0/16"]
  location                            = "${azurerm_resource_group.terraform2.location}"
  resource_group_name                 = "${azurerm_resource_group.terraform2.name}"

}

resource "azurerm_virtual_network_peering" "vn1tovn2" {
  name = "peer1to2"
  resource_group_name = "${azurerm_resource_group.terraform1.name}"
  virtual_network_name = "${azurerm_virtual_network.terraform1.name}"
  remote_virtual_network_id = "${azurerm_virtual_network.terraform2.id}"
}

resource "azurerm_virtual_network_peering" "vn2tovn1" {
  name                      = "peer2to1"
  resource_group_name       = "${azurerm_resource_group.terraform2.name}"
  virtual_network_name      = "${azurerm_virtual_network.terraform2.name}"
  remote_virtual_network_id = "${azurerm_virtual_network.terraform1.id}"
}

#subnet de la vm au japon
resource "azurerm_subnet" "terraform2" {
  name = "${var.name_subnet}"
  resource_group_name = "${azurerm_resource_group.terraform2.name}"
  virtual_network_name = "${azurerm_virtual_network.terraform2.name}"
  address_prefix = "10.1.10.0/24"
}


resource "azurerm_subnet" "terraform1" {
  name                                = "${var.name_subnet}"
  resource_group_name                 = "${azurerm_resource_group.terraform1.name}"
  virtual_network_name                = "${azurerm_virtual_network.terraform1.name}"
  address_prefix                      = "10.0.2.0/24"
}

resource "azurerm_network_security_group" "terraform2" {
  name                                = "${var.name_security_group}"
  location                            = "${azurerm_resource_group.terraform2.location}"
  resource_group_name                 = "${azurerm_resource_group.terraform2.name}"
  security_rule {
    name                              = "ssh"
    priority                          = 1001
    direction                         = "Inbound"
    access                            = "Allow"
    protocol                          = "Tcp"
    source_port_range                 = "*"
    destination_port_range            = "22"
    source_address_prefix             = "*"
    destination_address_prefix        = "*"
  }
  security_rule {
    name                              = "http"
    priority                          = 1002
    direction                         = "Inbound"
    access                            = "Allow"
    protocol                          = "Tcp"
    source_port_range                 = "*"
    destination_port_range            = "80"
    source_address_prefix             = "*"
    destination_address_prefix        = "*"
  }
}

resource "azurerm_network_security_group" "terraform1" {
  name                                = "${var.name_security_group}"
  location                            = "${azurerm_resource_group.terraform1.location}"
  resource_group_name                 = "${azurerm_resource_group.terraform1.name}"
  security_rule {
    name                              = "ssh"
    priority                          = 1001
    direction                         = "Inbound"
    access                            = "Allow"
    protocol                          = "Tcp"
    source_port_range                 = "*"
    destination_port_range            = "22"
    source_address_prefix             = "*"
    destination_address_prefix        = "*"
  }
  security_rule {
    name                              = "http"
    priority                          = 1002
    direction                         = "Inbound"
    access                            = "Allow"
    protocol                          = "Tcp"
    source_port_range                 = "*"
    destination_port_range            = "80"
    source_address_prefix             = "*"
    destination_address_prefix        = "*"
  }
}
resource "azurerm_public_ip" "terraform1" {
  name                                = "${var.name_public_ip}"
  location                            = "${azurerm_resource_group.terraform1.location}"
  resource_group_name                 = "${azurerm_resource_group.terraform1.name}"
  public_ip_address_allocation        = "dynamic"
}
 #public ip de la vm 5 chez au japon
resource "azurerm_public_ip" "terraform2" {
  name                                = "publicipdevm5"
  location                            = "${azurerm_resource_group.terraform2.location}"
  resource_group_name                 = "${azurerm_resource_group.terraform2.name}"
  public_ip_address_allocation        = "dynamic"
}


resource "azurerm_network_interface" "network_interface1" {
  name = "${var.name_network_interface1}"
  location = "${azurerm_resource_group.terraform1.location}"
  resource_group_name = "${azurerm_resource_group.terraform1.name}"
  network_security_group_id = "${azurerm_network_security_group.terraform1.id}"

  ip_configuration {
    name = "${var.name_ip_config_interface1}"
    subnet_id = "${azurerm_subnet.terraform1.id}"
    private_ip_address_allocation = "static"
    private_ip_address = "${var.private_ip}"
    public_ip_address_id = "${azurerm_public_ip.terraform1.id}"
  }
}
# network interface de la vm 5 au japon
 resource "azurerm_network_interface" "network_interface3" {
    name                                = "networinterfacevm5"
    location                            = "${azurerm_resource_group.terraform2.location}"
    resource_group_name                 = "${azurerm_resource_group.terraform2.name}"
    network_security_group_id           = "${azurerm_network_security_group.terraform2.id}"

    ip_configuration {
      name = "configipvm3"
      subnet_id = "${azurerm_subnet.terraform2.id}"
      private_ip_address_allocation = "static"
      private_ip_address = "10.0.2.24"
      public_ip_address_id = "${azurerm_public_ip.terraform2.id}"
    }

}
  resource "azurerm_network_interface" "network_interface2" {
    count                             = 3
    name                              = "${var.name_network_interface2}-${count.index+1}"
    location                          = "${azurerm_resource_group.terraform1.location}"
    resource_group_name               = "${azurerm_resource_group.terraform1.name}"
    network_security_group_id         = "${azurerm_network_security_group.terraform1.id}"
    ip_configuration {
      name                            = "${var.name_ip_config_interface2}"
      subnet_id                       = "${azurerm_subnet.terraform1.id}"
      private_ip_address_allocation   = "static"
      private_ip_address              = "${element(var.network_interface, count.index)}"
    }
  }

  resource "random_id" "randomId" {
    keepers = {
      # Generate a new ID only when a new resource group is defined
      resource_group                 = "${azurerm_resource_group.terraform1.name}"
    }

    byte_length                      = 8
  }
  resource "azurerm_storage_account" "newstorageaccountgtm" {
    name                             = "${var.name_storage_account}"
    resource_group_name              = "${azurerm_resource_group.terraform1.name}"
    location                         = "East US"
    account_replication_type         = "LRS"
    account_tier                     = "Standard"
  }

resource "azurerm_virtual_machine" "VM_public" {
  name = "${var.virtual_machina_public1}"
  location = "East US"
  resource_group_name = "${azurerm_resource_group.terraform1.name}"
  network_interface_ids = [
    "${azurerm_network_interface.network_interface1.id}"]
  vm_size = "Standard_DS1_V2"

  storage_os_disk {
    name = "${var.name_storage_disk_public_vm}"
    caching = "ReadWrite"
    create_option = "FromImage"
    managed_disk_type = "Premium_LRS"
  }
  storage_image_reference {
    publisher = "Canonical"
    offer = "UbuntuServer"
    sku = "16.04.0-LTS"
    version = "latest"
  }
  os_profile {
    computer_name = "VmDeOuf"
    admin_username = "${var.admin_username}"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path = "${var.path_ssh_key}"
      key_data = "${var.ssh_key}"
    }
  }
  boot_diagnostics {
    enabled = "true"
    storage_uri = "${azurerm_storage_account.newstorageaccountgtm.primary_blob_endpoint}"
  }

}
# la vm public vm5 au japon

resource "azurerm_virtual_machine" "VM_public2" {
    name                               = "vmaujapon"
    location                           = "Japan East"
    resource_group_name                = "${azurerm_resource_group.terraform2.name}"
    network_interface_ids              = ["${azurerm_network_interface.network_interface3.id}"]
    vm_size                            = "Standard_DS1_V2"

   storage_os_disk {
     name                             = "stockage1"
      caching                          = "ReadWrite"
      create_option                    = "FromImage"
      managed_disk_type                = "Premium_LRS"
    }
    storage_image_reference {
      publisher                        = "Canonical"
      offer                            = "UbuntuServer"
      sku                              = "16.04.0-LTS"
      version                          = "latest"
    }
    os_profile {
      computer_name                    = "VmDeOuf2"
      admin_username                   = "${var.admin_username}"
    }
    os_profile_linux_config {
      disable_password_authentication  = true
     ssh_keys {
        path                           = "${var.path_ssh_key}"
        key_data                       = "${var.ssh_key}"
      }
    }
    boot_diagnostics {
     enabled                          = "true"
      storage_uri                      = "${azurerm_storage_account.newstorageaccountgtm.primary_blob_endpoint}"
    }

}


resource "azurerm_virtual_machine" "VM_private" {
  count                              = 3
  name                               = "${var.name_virtual_machine_private}${count.index+1}"
  location                           = "East US"
  resource_group_name                = "${azurerm_resource_group.terraform1.name}"
  network_interface_ids              = ["${element(azurerm_network_interface.network_interface2.*.id, count.index)}"]
  vm_size                            = "Standard_DS1_V2"

  storage_os_disk {
    name                             = "${var.name_storage_disk_private_vm}${count.index+1}"
    caching                          = "ReadWrite"
    create_option                    = "FromImage"
    managed_disk_type                = "Premium_LRS"
  }
  storage_image_reference {
    publisher                        = "Canonical"
    offer                            = "UbuntuServer"
    sku                              = "16.04.0-LTS"
    version                          = "latest"
  }
  os_profile {
    computer_name                    = "stage"
    admin_username                   = "${var.user_name}"
  }
  os_profile_linux_config {
    disable_password_authentication  = true
    ssh_keys {
      path                           = "${var.path_ssh_key}"
      key_data                       = "${var.ssh_key}"
    }
  }
  boot_diagnostics {
    enabled                          = "true"
    storage_uri                      = "${azurerm_storage_account.newstorageaccountgtm.primary_blob_endpoint}"
  }
}

