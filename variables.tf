variable "subscription_id"{}
variable  "client_id"{}
variable "client_secret"{}
variable "tenant_id"{}
variable "private_ip"{}
variable "network_interface" {type= "list"}
variable "ssh_key" {}
variable "user_name" {}
variable "location_group_resource1" {}
variable "location_group_resource2" {}
variable "name_resource_groupe1" {}
variable "name_resource_groupe2" {}
variable "name_virtual_network_terraform1" {}
variable "name_virtual_network_terraform2" {}
variable "name_subnet" {}
variable "name_security_group"{}
variable "name_public_ip" {}
variable "name_network_interface1" {}
variable "name_ip_config_interface1" {}
variable "name_network_interface2" {}
variable "name_ip_config_interface2" {}
variable "name_storage_account" {}
variable "virtual_machina_public1" {}
variable "name_storage_disk_public_vm" {}
variable "admin_username" {}
variable "path_ssh_key" {}
variable "name_virtual_machine_private" {}
variable "name_storage_disk_private_vm" {}